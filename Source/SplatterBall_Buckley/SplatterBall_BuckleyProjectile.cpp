// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SplatterBall_BuckleyProjectile.h"
#include "SplatterBall_BuckleyCharacter.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "Goal.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/DecalActor.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"

ASplatterBall_BuckleyProjectile::ASplatterBall_BuckleyProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ASplatterBall_BuckleyProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Set as root component
	RootComponent = CollisionComp;

	// Create the Sphere Mesh sub component
	SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Sphere Mesh"));
	// Attach Sphere Mesh to root component so it displays
	SphereMesh->SetupAttachment(RootComponent);
	// Get Static Mesh 
	static ConstructorHelpers::FObjectFinder<UStaticMesh>StaticMesh1(TEXT("StaticMesh'/Game/FirstPerson/Meshes/FirstPersonProjectileMesh.FirstPersonProjectileMesh'"));

	// Set the static mesh to the SphereMesh
	SphereMesh->SetStaticMesh(StaticMesh1.Object);
	SphereMesh->SetRelativeScale3D(FVector(0.1, 0.1, 0.1));
	// Set the material for the SphereMesh to our material
	static ConstructorHelpers::FObjectFinder<UMaterial>ProjectileMaterial(TEXT("Material'/Game/FirstPersonCPP/Blueprints/ProjectileColor.ProjectileColor'"));

	// Set material for assigning to dynamic instance material
	UMaterial* MeshMaterial = ProjectileMaterial.Object;
	// Create the dynamic material instance
	UMaterialInstanceDynamic* ProjDynamic = UMaterialInstanceDynamic::Create(MeshMaterial, SphereMesh);

	// Set the projectile mesh material to be the DMI
	SphereMesh->SetMaterial(0, ProjDynamic);

	// Players can't walk on Collision Component
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Get random colors for projectile and decal
	ProjR = UKismetMathLibrary::RandomFloatInRange(0.000f, 1.0f);
	ProjG = UKismetMathLibrary::RandomFloatInRange(0.000f, 1.0f);
	ProjB = UKismetMathLibrary::RandomFloatInRange(0.000f, 1.0f);

	// Set the Vector Parameter Value in the new material 
	ProjDynamic->SetVectorParameterValue("ProjColor", FLinearColor(ProjR, ProjG, ProjB));

	// Find the material from our folder and if no error assign it to DecalMat
	static ConstructorHelpers::FObjectFinder<UMaterial>Material(TEXT("Material'/Game/Materials/Paint_Splat_M.Paint_Splat_M'"));
	if (Material.Object != NULL)
	{
		DecalMat = (UMaterial*)Material.Object;
	}


	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

}

void ASplatterBall_BuckleyProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	// Only add impulse and destroy projectile if we hit a physics
	// if ((OtherActor != NULL) && (OtherActor != this) && (OtherComp != NULL) && OtherComp->IsSimulatingPhysics())
	// {
	//	OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
	//
	//	Destroy();
	//}

	// Check if what we hit is ourself or null
	if ((OtherActor != NULL) && (OtherActor != this))
	{
		// Check we have a valid decal
		if (DecalMat != nullptr)
		{
			// Spawn the splat decal at the world location of the hit
			auto Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), DecalMat, FVector(UKismetMathLibrary::RandomFloatInRange(20.0f, 40.0f)), Hit.Location, Hit.Normal.Rotation(), 0.f);
			// Spawn the dynamic material instance
			auto MatInstance = Decal->CreateDynamicMaterialInstance();

			// Use random to grab one of the 4 types of decal
			MatInstance->SetScalarParameterValue("Frame", UKismetMathLibrary::RandomIntegerInRange(0, 3));
			// Use random to determine the color of the decal
			MatInstance->SetVectorParameterValue("Color", FLinearColor(ProjR, ProjG, ProjB));

			// Destroy the projectile as it isn't needed
			Destroy();
		}
	}
}