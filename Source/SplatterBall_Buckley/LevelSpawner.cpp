// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelSpawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Engine/EngineTypes.h"
#include "RuntimeCubeActor.h"

// Sets default values
ALevelSpawner::ALevelSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the Bounding Box used to constrain the random generation
	boxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	// Set the box to have a root component
	RootComponent = boxComp;
	// Assign default values to boxExtent
	boxExtent = FVector(400.0f, 400.0f, 10.0f);
	// Set the bounding box to the defined size values
	boxComp->SetBoxExtent(boxExtent);
}

// Called when the game starts or when spawned
void ALevelSpawner::BeginPlay()
{
	Super::BeginPlay();

	// Keep spawning walls every 5 seconds
	FTimerHandle WallSpawnHandle;

	GetWorldTimerManager().SetTimer(WallSpawnHandle, this, &ALevelSpawner::GenerateWalls, 5.0f, true, 1.0f);
	
}

// Called every frame
void ALevelSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALevelSpawner::GenerateWalls()
{
	// Counter for no. of Walls in a wave
	int BlockCount = 0;
	//Generate random Walls
	while (BlockCount <= 3)
	{
		// Assign Origin value to actor location in the level
		Origin = ALevelSpawner::GetActorLocation();
		// Initialize location of where to place wall
		FVector Position;
		// Get a random location from inside the bounding box
		Position = UKismetMathLibrary::RandomPointInBoundingBox(Origin, boxExtent);
		// Initialize the rotation of the wall
		FRotator Rotation;
		// Set any spawn params (Possible to add no overlap rules etc.)
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		// Spawn a wall generated from Runtime cube component plugin at the location chosen 
		GetWorld()->SpawnActor<ARuntimeCubeActor>(ARuntimeCubeActor::StaticClass(), Position, Rotation, SpawnParams);
		// Increment no. of Walls in the level
		BlockCount++;
	}
}

