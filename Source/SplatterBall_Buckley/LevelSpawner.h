// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "LevelSpawner.generated.h"

UCLASS()
class SPLATTERBALL_BUCKLEY_API ALevelSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelSpawner();

	// Function called to generate walls to spawn
	UFUNCTION()
		void GenerateWalls();
	
	// Origin point of bounding box
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Origin;

	// Extents of the box (for size)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector boxExtent;

	// Bounding box object in game
	UPROPERTY(EditAnywhere, Category = "Spawn")
		UBoxComponent* boxComp;

	// Game over flag
	UPROPERTY(EditAnywhere, Category = "Spawn")
		bool GameOver;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
