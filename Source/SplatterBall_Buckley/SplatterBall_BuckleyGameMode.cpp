// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SplatterBall_BuckleyGameMode.h"
#include "SplatterBall_BuckleyHUD.h"
#include "SplatterBall_BuckleyCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASplatterBall_BuckleyGameMode::ASplatterBall_BuckleyGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASplatterBall_BuckleyHUD::StaticClass();
}
