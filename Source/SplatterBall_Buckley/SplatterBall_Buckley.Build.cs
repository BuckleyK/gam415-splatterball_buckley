// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class SplatterBall_Buckley : ModuleRules
{
	public SplatterBall_Buckley(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "ProceduralMeshComponent", "RuntimeMeshComponent", "HeadMountedDisplay", "RHI", "RenderCore" });
    }
}
