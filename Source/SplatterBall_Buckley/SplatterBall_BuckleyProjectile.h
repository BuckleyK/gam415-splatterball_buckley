// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "SplatterBall_BuckleyProjectile.generated.h"

UCLASS(config=Game)
class ASplatterBall_BuckleyProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component **/
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component **/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

	/** Sphere Mesh for new projectile **/
	UPROPERTY(Editanywhere)
	class UStaticMeshComponent* SphereMesh;

public:
	ASplatterBall_BuckleyProjectile();

	/**Material to create ingame dynamic material from **/
	UPROPERTY(Editanywhere)
		UMaterial* DecalMat;

	/** Variables to store random color vector **/
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Color)
		float ProjR;
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Color)
		float ProjG;
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = Color)
		float ProjB;

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};

