// Fill out your copyright notice in the Description page of Project Settings.

#include "CubeSpawner.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/EngineTypes.h"


// Sets default values
ACubeSpawner::ACubeSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create the Bounding Box used to constrain the random generation
	boxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	// Set the box to have a root component
	RootComponent = boxComp;
	// Assign default values to boxExtent
	boxExtent = FVector(1100.0f, 1000.0f, 10.0f);
	// Set the bounding box to the defined size values
	boxComp->SetBoxExtent(boxExtent);
	
	// Get blueprint file for the cubes to spawn and assign to a variable
	static ConstructorHelpers::FObjectFinder<UClass> GameCubeBP(TEXT("Blueprint'/Game/SplatterBall/CubeAI.CubeAI_C'"));
	if (GameCubeBP.Succeeded() == true)
	{
		GameCube = GameCubeBP.Object;
	}

}

// Called when the game starts or when spawned
void ACubeSpawner::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACubeSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACubeSpawner::SpawnCubes()
{


	// Get a random number for amount of cubes in a wave from 3 to 7
	CubeCount = UKismetMathLibrary::RandomIntegerInRange(5, 8);

	// Assign Origin value to actor location in the level
	Origin = ACubeSpawner::GetActorLocation();

	// Initialize location of where to place cube
	FVector Position;

	// Counter for spawning cubes
	int Counter = 0;

	//Generate randomly placed cubes upto CubeCount
	while (Counter < CubeCount)
	{
		// Get a random location from inside the bounding box
		Position = UKismetMathLibrary::RandomPointInBoundingBox(Origin, boxExtent);
		// Initialize the rotation of the cube
		FRotator Rotation;
		// Set any spawn params (Possible to add no overlap rules etc.)
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

		// Spawn a cube generated from AI blueprint at the location chosen 
		GetWorld()->SpawnActor<AActor>(GameCube, Position, Rotation, SpawnParams);

		// Increment no. of Cube being spawned in the wave
		Counter++;
	}
}