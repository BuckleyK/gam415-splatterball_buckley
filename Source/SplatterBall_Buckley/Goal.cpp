// Fill out your copyright notice in the Description page of Project Settings.

#include "Goal.h"


// Sets default values
AGoal::AGoal()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Initialize values for Scoring
	//ScoreTotal = 0;
	//coreShootCube = 10;
	//CubeGoal = 5;
	//ScoreNextLevel = 250;
	//Wave = 1;
}

// Called when the game starts or when spawned
void AGoal::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGoal::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

