// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Goal.generated.h"

UCLASS()
class SPLATTERBALL_BUCKLEY_API AGoal : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGoal();

	// Score total
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
		int ScoreTotal;
	// Score amount for shooting correct block
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
		int ScoreShootCube;
	// Score amount for cube getting to goal
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
		int CubeGoal;
	// Score total to complete level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
		int ScoreNextLevel;

	// Score total to complete level
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
		int Wave;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
