// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Components/BoxComponent.h"
#include "CubeSpawner.generated.h"

UCLASS()
class SPLATTERBALL_BUCKLEY_API ACubeSpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACubeSpawner();

	// Function called to Spawn walls in the new level
	UFUNCTION(BlueprintCallable)
		void SpawnCubes();

	// Origin point of bounding box
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Origin;

	// Extents of the box (for size)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector boxExtent;

	// Bounding box object in game
	UPROPERTY(EditAnywhere, Category = "Spawn")
		UBoxComponent* boxComp;
	
	// Create link to AI blueprint
	UPROPERTY(EditAnywhere, category = "AI")
		TSubclassOf<AActor> GameCube;

	/** Variables to store random color vector **/
	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = "Color")
		float ProjR;
	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = "Color")
		float ProjG;
	UPROPERTY(Editanywhere, BlueprintReadOnly, Category = "Color")
		float ProjB;

	// variable for number of spawn waves in a Wave
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = "Spawn")
		int NumSpawnwavesInWave = 3;

	// variable for Wave number
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = "Spawn")
		int WaveNumber = 1;

	// variable for number of Cubes in a spawn wave 
	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = "Spawn")
		int CubeCount = 4;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
