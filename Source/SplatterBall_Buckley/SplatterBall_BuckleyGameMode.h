// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SplatterBall_BuckleyGameMode.generated.h"

UCLASS(minimalapi)
class ASplatterBall_BuckleyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASplatterBall_BuckleyGameMode();
};



