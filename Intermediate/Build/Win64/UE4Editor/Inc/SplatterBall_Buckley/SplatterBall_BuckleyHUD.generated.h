// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyHUD_generated_h
#error "SplatterBall_BuckleyHUD.generated.h already included, missing '#pragma once' in SplatterBall_BuckleyHUD.h"
#endif
#define SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyHUD_generated_h

#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_RPC_WRAPPERS
#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyHUD(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyHUD(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyHUD) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatterBall_BuckleyHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatterBall_BuckleyHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyHUD(ASplatterBall_BuckleyHUD&&); \
	NO_API ASplatterBall_BuckleyHUD(const ASplatterBall_BuckleyHUD&); \
public:


#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyHUD(ASplatterBall_BuckleyHUD&&); \
	NO_API ASplatterBall_BuckleyHUD(const ASplatterBall_BuckleyHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatterBall_BuckleyHUD)


#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_9_PROLOG
#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_RPC_WRAPPERS \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_INCLASS \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_INCLASS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Final_Project_Test_Source_SplatterBall_Buckley_SplatterBall_BuckleyHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
