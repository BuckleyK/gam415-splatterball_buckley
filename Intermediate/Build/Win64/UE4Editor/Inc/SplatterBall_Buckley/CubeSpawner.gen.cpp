// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "CubeSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCubeSpawner() {}
// Cross Module References
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ACubeSpawner_NoRegister();
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ACubeSpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SplatterBall_Buckley();
	SPLATTERBALL_BUCKLEY_API UFunction* Z_Construct_UFunction_ACubeSpawner_SpawnCubes();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ACubeSpawner::StaticRegisterNativesACubeSpawner()
	{
		UClass* Class = ACubeSpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SpawnCubes", (Native)&ACubeSpawner::execSpawnCubes },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ACubeSpawner_SpawnCubes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Function called to Spawn walls in the new level" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ACubeSpawner, "SpawnCubes", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACubeSpawner_NoRegister()
	{
		return ACubeSpawner::StaticClass();
	}
	UClass* Z_Construct_UClass_ACubeSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_SplatterBall_Buckley,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ACubeSpawner_SpawnCubes, "SpawnCubes" }, // 3285798968
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "CubeSpawner.h" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeCount_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "variable for number of Cubes in a spawn wave" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CubeCount = { UE4CodeGen_Private::EPropertyClass::Int, "CubeCount", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, CubeCount), METADATA_PARAMS(NewProp_CubeCount_MetaData, ARRAY_COUNT(NewProp_CubeCount_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaveNumber_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "variable for Wave number" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WaveNumber = { UE4CodeGen_Private::EPropertyClass::Int, "WaveNumber", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, WaveNumber), METADATA_PARAMS(NewProp_WaveNumber_MetaData, ARRAY_COUNT(NewProp_WaveNumber_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumSpawnwavesInWave_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "variable for number of spawn waves in a Wave" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_NumSpawnwavesInWave = { UE4CodeGen_Private::EPropertyClass::Int, "NumSpawnwavesInWave", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, NumSpawnwavesInWave), METADATA_PARAMS(NewProp_NumSpawnwavesInWave_MetaData, ARRAY_COUNT(NewProp_NumSpawnwavesInWave_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjB_MetaData[] = {
				{ "Category", "Color" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProjB = { UE4CodeGen_Private::EPropertyClass::Float, "ProjB", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000015, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, ProjB), METADATA_PARAMS(NewProp_ProjB_MetaData, ARRAY_COUNT(NewProp_ProjB_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjG_MetaData[] = {
				{ "Category", "Color" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProjG = { UE4CodeGen_Private::EPropertyClass::Float, "ProjG", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000015, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, ProjG), METADATA_PARAMS(NewProp_ProjG_MetaData, ARRAY_COUNT(NewProp_ProjG_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjR_MetaData[] = {
				{ "Category", "Color" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Variables to store random color vector *" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProjR = { UE4CodeGen_Private::EPropertyClass::Float, "ProjR", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000015, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, ProjR), METADATA_PARAMS(NewProp_ProjR_MetaData, ARRAY_COUNT(NewProp_ProjR_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameCube_MetaData[] = {
				{ "Category", "AI" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Create link to AI blueprint" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_GameCube = { UE4CodeGen_Private::EPropertyClass::Class, "GameCube", RF_Public|RF_Transient|RF_MarkAsNative, 0x0014000000000001, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, GameCube), Z_Construct_UClass_AActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_GameCube_MetaData, ARRAY_COUNT(NewProp_GameCube_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxComp_MetaData[] = {
				{ "Category", "Spawn" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Bounding box object in game" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_boxComp = { UE4CodeGen_Private::EPropertyClass::Object, "boxComp", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080009, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, boxComp), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(NewProp_boxComp_MetaData, ARRAY_COUNT(NewProp_boxComp_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxExtent_MetaData[] = {
				{ "Category", "CubeSpawner" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Extents of the box (for size)" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_boxExtent = { UE4CodeGen_Private::EPropertyClass::Struct, "boxExtent", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, boxExtent), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_boxExtent_MetaData, ARRAY_COUNT(NewProp_boxExtent_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[] = {
				{ "Category", "CubeSpawner" },
				{ "ModuleRelativePath", "CubeSpawner.h" },
				{ "ToolTip", "Origin point of bounding box" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_Origin = { UE4CodeGen_Private::EPropertyClass::Struct, "Origin", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ACubeSpawner, Origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_Origin_MetaData, ARRAY_COUNT(NewProp_Origin_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CubeCount,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_WaveNumber,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_NumSpawnwavesInWave,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ProjB,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ProjG,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ProjR,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_GameCube,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxComp,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxExtent,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Origin,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ACubeSpawner>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ACubeSpawner::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACubeSpawner, 1555563125);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACubeSpawner(Z_Construct_UClass_ACubeSpawner, &ACubeSpawner::StaticClass, TEXT("/Script/SplatterBall_Buckley"), TEXT("ACubeSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACubeSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
