// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "SplatterBall_BuckleyHUD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplatterBall_BuckleyHUD() {}
// Cross Module References
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD_NoRegister();
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD();
	ENGINE_API UClass* Z_Construct_UClass_AHUD();
	UPackage* Z_Construct_UPackage__Script_SplatterBall_Buckley();
// End Cross Module References
	void ASplatterBall_BuckleyHUD::StaticRegisterNativesASplatterBall_BuckleyHUD()
	{
	}
	UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD_NoRegister()
	{
		return ASplatterBall_BuckleyHUD::StaticClass();
	}
	UClass* Z_Construct_UClass_ASplatterBall_BuckleyHUD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AHUD,
				(UObject* (*)())Z_Construct_UPackage__Script_SplatterBall_Buckley,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Rendering Actor Input Replication" },
				{ "IncludePath", "SplatterBall_BuckleyHUD.h" },
				{ "ModuleRelativePath", "SplatterBall_BuckleyHUD.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASplatterBall_BuckleyHUD>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASplatterBall_BuckleyHUD::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x0080028Cu,
				nullptr, 0,
				nullptr, 0,
				"Game",
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASplatterBall_BuckleyHUD, 3932784204);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASplatterBall_BuckleyHUD(Z_Construct_UClass_ASplatterBall_BuckleyHUD, &ASplatterBall_BuckleyHUD::StaticClass, TEXT("/Script/SplatterBall_Buckley"), TEXT("ASplatterBall_BuckleyHUD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASplatterBall_BuckleyHUD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
