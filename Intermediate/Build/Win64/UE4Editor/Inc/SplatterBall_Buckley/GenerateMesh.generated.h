// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_GenerateMesh_generated_h
#error "GenerateMesh.generated.h already included, missing '#pragma once' in GenerateMesh.h"
#endif
#define SPLATTERBALL_BUCKLEY_GenerateMesh_generated_h

#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_RPC_WRAPPERS
#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGenerateMesh(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_AGenerateMesh(); \
public: \
	DECLARE_CLASS(AGenerateMesh, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(AGenerateMesh) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAGenerateMesh(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_AGenerateMesh(); \
public: \
	DECLARE_CLASS(AGenerateMesh, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(AGenerateMesh) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGenerateMesh(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGenerateMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGenerateMesh); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGenerateMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGenerateMesh(AGenerateMesh&&); \
	NO_API AGenerateMesh(const AGenerateMesh&); \
public:


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGenerateMesh(AGenerateMesh&&); \
	NO_API AGenerateMesh(const AGenerateMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGenerateMesh); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGenerateMesh); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGenerateMesh)


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(AGenerateMesh, mesh); }


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_11_PROLOG
#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_RPC_WRAPPERS \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_INCLASS \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_INCLASS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Final_Project_Test_Source_SplatterBall_Buckley_GenerateMesh_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
