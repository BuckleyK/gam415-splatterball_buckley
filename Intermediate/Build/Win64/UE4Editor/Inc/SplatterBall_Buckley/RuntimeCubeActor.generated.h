// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_RuntimeCubeActor_generated_h
#error "RuntimeCubeActor.generated.h already included, missing '#pragma once' in RuntimeCubeActor.h"
#endif
#define SPLATTERBALL_BUCKLEY_RuntimeCubeActor_generated_h

#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS
#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ARuntimeCubeActor(); \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ARuntimeCubeActor(); \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARuntimeCubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARuntimeCubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public:


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARuntimeCubeActor)


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(ARuntimeCubeActor, Material); } \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ARuntimeCubeActor, mesh); }


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_10_PROLOG
#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS_NO_PURE_DECLS \
	Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Final_Project_Test_Source_SplatterBall_Buckley_RuntimeCubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
