// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "SplatterBall_BuckleyGameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSplatterBall_BuckleyGameMode() {}
// Cross Module References
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ASplatterBall_BuckleyGameMode_NoRegister();
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ASplatterBall_BuckleyGameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_SplatterBall_Buckley();
// End Cross Module References
	void ASplatterBall_BuckleyGameMode::StaticRegisterNativesASplatterBall_BuckleyGameMode()
	{
	}
	UClass* Z_Construct_UClass_ASplatterBall_BuckleyGameMode_NoRegister()
	{
		return ASplatterBall_BuckleyGameMode::StaticClass();
	}
	UClass* Z_Construct_UClass_ASplatterBall_BuckleyGameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AGameModeBase,
				(UObject* (*)())Z_Construct_UPackage__Script_SplatterBall_Buckley,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
				{ "IncludePath", "SplatterBall_BuckleyGameMode.h" },
				{ "ModuleRelativePath", "SplatterBall_BuckleyGameMode.h" },
				{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
			};
#endif
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ASplatterBall_BuckleyGameMode>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ASplatterBall_BuckleyGameMode::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00880288u,
				nullptr, 0,
				nullptr, 0,
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASplatterBall_BuckleyGameMode, 2936675369);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASplatterBall_BuckleyGameMode(Z_Construct_UClass_ASplatterBall_BuckleyGameMode, &ASplatterBall_BuckleyGameMode::StaticClass, TEXT("/Script/SplatterBall_Buckley"), TEXT("ASplatterBall_BuckleyGameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASplatterBall_BuckleyGameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
