// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyProjectile_generated_h
#error "SplatterBall_BuckleyProjectile.generated.h already included, missing '#pragma once' in SplatterBall_BuckleyProjectile.h"
#endif
#define SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyProjectile_generated_h

#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyProjectile(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyProjectile(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_INCLASS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyProjectile(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyProjectile(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyProjectile, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyProjectile) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC}; \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatterBall_BuckleyProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatterBall_BuckleyProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyProjectile(ASplatterBall_BuckleyProjectile&&); \
	NO_API ASplatterBall_BuckleyProjectile(const ASplatterBall_BuckleyProjectile&); \
public:


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyProjectile(ASplatterBall_BuckleyProjectile&&); \
	NO_API ASplatterBall_BuckleyProjectile(const ASplatterBall_BuckleyProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatterBall_BuckleyProjectile)


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ASplatterBall_BuckleyProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ASplatterBall_BuckleyProjectile, ProjectileMovement); } \
	FORCEINLINE static uint32 __PPO__SphereMesh() { return STRUCT_OFFSET(ASplatterBall_BuckleyProjectile, SphereMesh); }


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_10_PROLOG
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_RPC_WRAPPERS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_INCLASS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_INCLASS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
