// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_CubeSpawner_generated_h
#error "CubeSpawner.generated.h already included, missing '#pragma once' in CubeSpawner.h"
#endif
#define SPLATTERBALL_BUCKLEY_CubeSpawner_generated_h

#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSpawnCubes) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnCubes(); \
		P_NATIVE_END; \
	}


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSpawnCubes) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnCubes(); \
		P_NATIVE_END; \
	}


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACubeSpawner(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ACubeSpawner(); \
public: \
	DECLARE_CLASS(ACubeSpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ACubeSpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_INCLASS \
private: \
	static void StaticRegisterNativesACubeSpawner(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ACubeSpawner(); \
public: \
	DECLARE_CLASS(ACubeSpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ACubeSpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACubeSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACubeSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeSpawner(ACubeSpawner&&); \
	NO_API ACubeSpawner(const ACubeSpawner&); \
public:


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACubeSpawner(ACubeSpawner&&); \
	NO_API ACubeSpawner(const ACubeSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACubeSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACubeSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACubeSpawner)


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_PRIVATE_PROPERTY_OFFSET
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_11_PROLOG
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_RPC_WRAPPERS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_INCLASS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_INCLASS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SplatterBall_Buckley_Source_SplatterBall_Buckley_CubeSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
