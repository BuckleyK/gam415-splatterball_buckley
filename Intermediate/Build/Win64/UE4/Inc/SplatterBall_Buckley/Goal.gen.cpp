// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Goal.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGoal() {}
// Cross Module References
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_AGoal_NoRegister();
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_AGoal();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SplatterBall_Buckley();
// End Cross Module References
	void AGoal::StaticRegisterNativesAGoal()
	{
	}
	UClass* Z_Construct_UClass_AGoal_NoRegister()
	{
		return AGoal::StaticClass();
	}
	UClass* Z_Construct_UClass_AGoal()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_SplatterBall_Buckley,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "Goal.h" },
				{ "ModuleRelativePath", "Goal.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Wave_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Goal.h" },
				{ "ToolTip", "Score total to complete level" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Wave = { UE4CodeGen_Private::EPropertyClass::Int, "Wave", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AGoal, Wave), METADATA_PARAMS(NewProp_Wave_MetaData, ARRAY_COUNT(NewProp_Wave_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoreNextLevel_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Goal.h" },
				{ "ToolTip", "Score total to complete level" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ScoreNextLevel = { UE4CodeGen_Private::EPropertyClass::Int, "ScoreNextLevel", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AGoal, ScoreNextLevel), METADATA_PARAMS(NewProp_ScoreNextLevel_MetaData, ARRAY_COUNT(NewProp_ScoreNextLevel_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CubeGoal_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Goal.h" },
				{ "ToolTip", "Score amount for cube getting to goal" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CubeGoal = { UE4CodeGen_Private::EPropertyClass::Int, "CubeGoal", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AGoal, CubeGoal), METADATA_PARAMS(NewProp_CubeGoal_MetaData, ARRAY_COUNT(NewProp_CubeGoal_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoreShootCube_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Goal.h" },
				{ "ToolTip", "Score amount for shooting correct block" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ScoreShootCube = { UE4CodeGen_Private::EPropertyClass::Int, "ScoreShootCube", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AGoal, ScoreShootCube), METADATA_PARAMS(NewProp_ScoreShootCube_MetaData, ARRAY_COUNT(NewProp_ScoreShootCube_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScoreTotal_MetaData[] = {
				{ "Category", "Score" },
				{ "ModuleRelativePath", "Goal.h" },
				{ "ToolTip", "Score total" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ScoreTotal = { UE4CodeGen_Private::EPropertyClass::Int, "ScoreTotal", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AGoal, ScoreTotal), METADATA_PARAMS(NewProp_ScoreTotal_MetaData, ARRAY_COUNT(NewProp_ScoreTotal_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Wave,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScoreNextLevel,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_CubeGoal,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScoreShootCube,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ScoreTotal,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AGoal>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AGoal::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				nullptr, 0,
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGoal, 3361837247);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGoal(Z_Construct_UClass_AGoal, &AGoal::StaticClass, TEXT("/Script/SplatterBall_Buckley"), TEXT("AGoal"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGoal);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
