// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyCharacter_generated_h
#error "SplatterBall_BuckleyCharacter.generated.h already included, missing '#pragma once' in SplatterBall_BuckleyCharacter.h"
#endif
#define SPLATTERBALL_BUCKLEY_SplatterBall_BuckleyCharacter_generated_h

#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_RPC_WRAPPERS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyCharacter(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyCharacter(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASplatterBall_BuckleyCharacter(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASplatterBall_BuckleyCharacter(); \
public: \
	DECLARE_CLASS(ASplatterBall_BuckleyCharacter, ACharacter, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASplatterBall_BuckleyCharacter) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASplatterBall_BuckleyCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASplatterBall_BuckleyCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyCharacter(ASplatterBall_BuckleyCharacter&&); \
	NO_API ASplatterBall_BuckleyCharacter(const ASplatterBall_BuckleyCharacter&); \
public:


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASplatterBall_BuckleyCharacter(ASplatterBall_BuckleyCharacter&&); \
	NO_API ASplatterBall_BuckleyCharacter(const ASplatterBall_BuckleyCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASplatterBall_BuckleyCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASplatterBall_BuckleyCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASplatterBall_BuckleyCharacter)


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ASplatterBall_BuckleyCharacter, L_MotionController); }


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_11_PROLOG
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_RPC_WRAPPERS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_INCLASS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_INCLASS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SplatterBall_Buckley_Source_SplatterBall_Buckley_SplatterBall_BuckleyCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
