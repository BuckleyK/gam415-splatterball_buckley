// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_SpawnAICube_generated_h
#error "SpawnAICube.generated.h already included, missing '#pragma once' in SpawnAICube.h"
#endif
#define SPLATTERBALL_BUCKLEY_SpawnAICube_generated_h

#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_RPC_WRAPPERS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASpawnAICube(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASpawnAICube(); \
public: \
	DECLARE_CLASS(ASpawnAICube, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASpawnAICube) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_INCLASS \
private: \
	static void StaticRegisterNativesASpawnAICube(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ASpawnAICube(); \
public: \
	DECLARE_CLASS(ASpawnAICube, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ASpawnAICube) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASpawnAICube(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASpawnAICube) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnAICube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnAICube); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnAICube(ASpawnAICube&&); \
	NO_API ASpawnAICube(const ASpawnAICube&); \
public:


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASpawnAICube(ASpawnAICube&&); \
	NO_API ASpawnAICube(const ASpawnAICube&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASpawnAICube); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASpawnAICube); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASpawnAICube)


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_PRIVATE_PROPERTY_OFFSET
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_9_PROLOG
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_RPC_WRAPPERS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_INCLASS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_INCLASS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SplatterBall_Buckley_Source_SplatterBall_Buckley_SpawnAICube_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
