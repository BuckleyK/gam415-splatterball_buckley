// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPLATTERBALL_BUCKLEY_RuntimeCubeActor_generated_h
#error "RuntimeCubeActor.generated.h already included, missing '#pragma once' in RuntimeCubeActor.h"
#endif
#define SPLATTERBALL_BUCKLEY_RuntimeCubeActor_generated_h

#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ARuntimeCubeActor(); \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesARuntimeCubeActor(); \
	friend SPLATTERBALL_BUCKLEY_API class UClass* Z_Construct_UClass_ARuntimeCubeActor(); \
public: \
	DECLARE_CLASS(ARuntimeCubeActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/SplatterBall_Buckley"), NO_API) \
	DECLARE_SERIALIZER(ARuntimeCubeActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARuntimeCubeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARuntimeCubeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public:


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARuntimeCubeActor(ARuntimeCubeActor&&); \
	NO_API ARuntimeCubeActor(const ARuntimeCubeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARuntimeCubeActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARuntimeCubeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARuntimeCubeActor)


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(ARuntimeCubeActor, Material); } \
	FORCEINLINE static uint32 __PPO__mesh() { return STRUCT_OFFSET(ARuntimeCubeActor, mesh); }


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_10_PROLOG
#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_PRIVATE_PROPERTY_OFFSET \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_INCLASS_NO_PURE_DECLS \
	SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SplatterBall_Buckley_Source_SplatterBall_Buckley_RuntimeCubeActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
