// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "LevelSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSpawner() {}
// Cross Module References
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ALevelSpawner_NoRegister();
	SPLATTERBALL_BUCKLEY_API UClass* Z_Construct_UClass_ALevelSpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SplatterBall_Buckley();
	SPLATTERBALL_BUCKLEY_API UFunction* Z_Construct_UFunction_ALevelSpawner_GenerateWalls();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void ALevelSpawner::StaticRegisterNativesALevelSpawner()
	{
		UClass* Class = ALevelSpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GenerateWalls", (Native)&ALevelSpawner::execGenerateWalls },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_ALevelSpawner_GenerateWalls()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "LevelSpawner.h" },
				{ "ToolTip", "Function called to generate walls to spawn" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_ALevelSpawner, "GenerateWalls", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ALevelSpawner_NoRegister()
	{
		return ALevelSpawner::StaticClass();
	}
	UClass* Z_Construct_UClass_ALevelSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_SplatterBall_Buckley,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_ALevelSpawner_GenerateWalls, "GenerateWalls" }, // 4276091058
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "LevelSpawner.h" },
				{ "ModuleRelativePath", "LevelSpawner.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameOver_MetaData[] = {
				{ "Category", "Spawn" },
				{ "ModuleRelativePath", "LevelSpawner.h" },
				{ "ToolTip", "Game over flag" },
			};
#endif
			auto NewProp_GameOver_SetBit = [](void* Obj){ ((ALevelSpawner*)Obj)->GameOver = 1; };
			static const UE4CodeGen_Private::FBoolPropertyParams NewProp_GameOver = { UE4CodeGen_Private::EPropertyClass::Bool, "GameOver", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000001, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ALevelSpawner), &UE4CodeGen_Private::TBoolSetBitWrapper<decltype(NewProp_GameOver_SetBit)>::SetBit, METADATA_PARAMS(NewProp_GameOver_MetaData, ARRAY_COUNT(NewProp_GameOver_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxComp_MetaData[] = {
				{ "Category", "Spawn" },
				{ "EditInline", "true" },
				{ "ModuleRelativePath", "LevelSpawner.h" },
				{ "ToolTip", "Bounding box object in game" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_boxComp = { UE4CodeGen_Private::EPropertyClass::Object, "boxComp", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000080009, 1, nullptr, STRUCT_OFFSET(ALevelSpawner, boxComp), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(NewProp_boxComp_MetaData, ARRAY_COUNT(NewProp_boxComp_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_boxExtent_MetaData[] = {
				{ "Category", "LevelSpawner" },
				{ "ModuleRelativePath", "LevelSpawner.h" },
				{ "ToolTip", "Extents of the box (for size)" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_boxExtent = { UE4CodeGen_Private::EPropertyClass::Struct, "boxExtent", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ALevelSpawner, boxExtent), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_boxExtent_MetaData, ARRAY_COUNT(NewProp_boxExtent_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[] = {
				{ "Category", "LevelSpawner" },
				{ "ModuleRelativePath", "LevelSpawner.h" },
				{ "ToolTip", "Origin point of bounding box" },
			};
#endif
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_Origin = { UE4CodeGen_Private::EPropertyClass::Struct, "Origin", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ALevelSpawner, Origin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(NewProp_Origin_MetaData, ARRAY_COUNT(NewProp_Origin_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_GameOver,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxComp,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_boxExtent,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Origin,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<ALevelSpawner>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&ALevelSpawner::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ALevelSpawner, 4237704133);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ALevelSpawner(Z_Construct_UClass_ALevelSpawner, &ALevelSpawner::StaticClass, TEXT("/Script/SplatterBall_Buckley"), TEXT("ALevelSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ALevelSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
