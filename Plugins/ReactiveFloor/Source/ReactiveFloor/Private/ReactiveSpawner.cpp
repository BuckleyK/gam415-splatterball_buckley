// Fill out your copyright notice in the Description page of Project Settings.

#include "ReactiveSpawner.h"
#include "Engine/World.h"

// Sets default values
AReactiveSpawner::AReactiveSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AReactiveSpawner::BeginPlay()
{
	Super::BeginPlay();

	// Spawn the object
	SetupSpawn();
	
}

// Called every frame
void AReactiveSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AReactiveSpawner::SetupSpawn()
{
	// Setup variables for location and rotation of the spawn
	FVector Loc (0.0f, 0.0f, 0.0f);
	FRotator Rot (0.0f, 0.0f, 0.0f);

	float GridX = 0.0f;
	float GridY = 0.0f;
	int i = 0;
	int j = 0;

	// Do some math to create the grid & location
	for (i = 0; i < MaxX; i++)
	{
		for (j = 0; j < MaxY; j++) 
		{
			GridX = float(i * Width);
			GridY = float(j * Length);

			// create loc to spawn grid subtracting where it needs to be in level
			Loc = FVector(GridX - 2400.0f, GridY - 1450.0f, -10.0f);

			// Call the spawn function
			SpawnObject(Loc, Rot);
		}
	}
}

void AReactiveSpawner::SpawnObject(FVector Loc, FRotator Rot)
{
	
	// Get the world 
	UWorld* world = GetWorld();
	// If the world is valid
	if (world)
	{
		// Set our spawn parameters
		FActorSpawnParameters SpawnParams;

		// Spawn the actor into the world
		AActor* SpawnedActorRef = world->SpawnActor<AActor>(ActorToSpawn, Loc, Rot, SpawnParams);
	}


		
}
