// Fill out your copyright notice in the Description page of Project Settings.

#include "ReactiveActor.h"


// Sets default values
AReactiveActor::AReactiveActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	//Create the Root component
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Comp"));
	// Assign the new Root to be the root component
	RootComponent = Root;

	// Create the mesh component
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh Comp"));
	// Attach the mesh component to the root
	Mesh->AttachToComponent(Root, FAttachmentTransformRules::SnapToTargetIncludingScale);

}

// Called when the game starts or when spawned
void AReactiveActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AReactiveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

