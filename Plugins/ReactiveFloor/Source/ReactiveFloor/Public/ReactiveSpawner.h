// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ReactiveActor.h"
#include "ReactiveSpawner.generated.h"

UCLASS()
class REACTIVEFLOOR_API AReactiveSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AReactiveSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Function to spawn the object
	UFUNCTION()
		void SpawnObject(FVector Loc, FRotator Rot);

	// Function to setup how to spawn
	UFUNCTION()
		void SetupSpawn();

	// Get the Actor to spawn
	UPROPERTY(EditAnywhere, Category = "Actor Spawn")
		TSubclassOf<class AReactiveActor> ActorToSpawn;
	
	// Maximum X direction (grid size)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MaxX = 5;

	// Maximum Y direction (grid size)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int MaxY = 5;

	// Width of Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Width = 100.0f;

	// Length of Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Length = 100.0f;
};
