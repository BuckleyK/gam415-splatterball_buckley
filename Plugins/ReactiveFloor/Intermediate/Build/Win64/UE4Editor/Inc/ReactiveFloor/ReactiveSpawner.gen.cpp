// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Public/ReactiveSpawner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReactiveSpawner() {}
// Cross Module References
	REACTIVEFLOOR_API UClass* Z_Construct_UClass_AReactiveSpawner_NoRegister();
	REACTIVEFLOOR_API UClass* Z_Construct_UClass_AReactiveSpawner();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ReactiveFloor();
	REACTIVEFLOOR_API UFunction* Z_Construct_UFunction_AReactiveSpawner_SetupSpawn();
	REACTIVEFLOOR_API UFunction* Z_Construct_UFunction_AReactiveSpawner_SpawnObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	REACTIVEFLOOR_API UClass* Z_Construct_UClass_AReactiveActor_NoRegister();
// End Cross Module References
	void AReactiveSpawner::StaticRegisterNativesAReactiveSpawner()
	{
		UClass* Class = AReactiveSpawner::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetupSpawn", (Native)&AReactiveSpawner::execSetupSpawn },
			{ "SpawnObject", (Native)&AReactiveSpawner::execSpawnObject },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_AReactiveSpawner_SetupSpawn()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Function to setup how to spawn" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AReactiveSpawner, "SetupSpawn", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_AReactiveSpawner_SpawnObject()
	{
		struct ReactiveSpawner_eventSpawnObject_Parms
		{
			FVector Loc;
			FRotator Rot;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rot = { UE4CodeGen_Private::EPropertyClass::Struct, "Rot", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ReactiveSpawner_eventSpawnObject_Parms, Rot), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FStructPropertyParams NewProp_Loc = { UE4CodeGen_Private::EPropertyClass::Struct, "Loc", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(ReactiveSpawner_eventSpawnObject_Parms, Loc), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Rot,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Loc,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Function to spawn the object" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_AReactiveSpawner, "SpawnObject", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x00820401, sizeof(ReactiveSpawner_eventSpawnObject_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AReactiveSpawner_NoRegister()
	{
		return AReactiveSpawner::StaticClass();
	}
	UClass* Z_Construct_UClass_AReactiveSpawner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_AActor,
				(UObject* (*)())Z_Construct_UPackage__Script_ReactiveFloor,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_AReactiveSpawner_SetupSpawn, "SetupSpawn" }, // 3807582410
				{ &Z_Construct_UFunction_AReactiveSpawner_SpawnObject, "SpawnObject" }, // 2303650316
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "IncludePath", "ReactiveSpawner.h" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Length_MetaData[] = {
				{ "Category", "ReactiveSpawner" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Length of Mesh" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Length = { UE4CodeGen_Private::EPropertyClass::Float, "Length", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AReactiveSpawner, Length), METADATA_PARAMS(NewProp_Length_MetaData, ARRAY_COUNT(NewProp_Length_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[] = {
				{ "Category", "ReactiveSpawner" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Width of Mesh" },
			};
#endif
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width = { UE4CodeGen_Private::EPropertyClass::Float, "Width", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AReactiveSpawner, Width), METADATA_PARAMS(NewProp_Width_MetaData, ARRAY_COUNT(NewProp_Width_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxY_MetaData[] = {
				{ "Category", "ReactiveSpawner" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Maximum Y direction (grid size)" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxY = { UE4CodeGen_Private::EPropertyClass::Int, "MaxY", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AReactiveSpawner, MaxY), METADATA_PARAMS(NewProp_MaxY_MetaData, ARRAY_COUNT(NewProp_MaxY_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxX_MetaData[] = {
				{ "Category", "ReactiveSpawner" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Maximum X direction (grid size)" },
			};
#endif
			static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxX = { UE4CodeGen_Private::EPropertyClass::Int, "MaxX", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000005, 1, nullptr, STRUCT_OFFSET(AReactiveSpawner, MaxX), METADATA_PARAMS(NewProp_MaxX_MetaData, ARRAY_COUNT(NewProp_MaxX_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorToSpawn_MetaData[] = {
				{ "Category", "Actor Spawn" },
				{ "ModuleRelativePath", "Public/ReactiveSpawner.h" },
				{ "ToolTip", "Get the Actor to spawn" },
			};
#endif
			static const UE4CodeGen_Private::FClassPropertyParams NewProp_ActorToSpawn = { UE4CodeGen_Private::EPropertyClass::Class, "ActorToSpawn", RF_Public|RF_Transient|RF_MarkAsNative, 0x0014000000000001, 1, nullptr, STRUCT_OFFSET(AReactiveSpawner, ActorToSpawn), Z_Construct_UClass_AReactiveActor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(NewProp_ActorToSpawn_MetaData, ARRAY_COUNT(NewProp_ActorToSpawn_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Length,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Width,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MaxY,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_MaxX,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_ActorToSpawn,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<AReactiveSpawner>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&AReactiveSpawner::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AReactiveSpawner, 327943259);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AReactiveSpawner(Z_Construct_UClass_AReactiveSpawner, &AReactiveSpawner::StaticClass, TEXT("/Script/ReactiveFloor"), TEXT("AReactiveSpawner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AReactiveSpawner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
