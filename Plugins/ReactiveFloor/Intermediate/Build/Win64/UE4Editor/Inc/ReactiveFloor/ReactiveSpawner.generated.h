// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FRotator;
#ifdef REACTIVEFLOOR_ReactiveSpawner_generated_h
#error "ReactiveSpawner.generated.h already included, missing '#pragma once' in ReactiveSpawner.h"
#endif
#define REACTIVEFLOOR_ReactiveSpawner_generated_h

#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetupSpawn) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupSpawn(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnObject) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_Loc); \
		P_GET_STRUCT(FRotator,Z_Param_Rot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnObject(Z_Param_Loc,Z_Param_Rot); \
		P_NATIVE_END; \
	}


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetupSpawn) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SetupSpawn(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnObject) \
	{ \
		P_GET_STRUCT(FVector,Z_Param_Loc); \
		P_GET_STRUCT(FRotator,Z_Param_Rot); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnObject(Z_Param_Loc,Z_Param_Rot); \
		P_NATIVE_END; \
	}


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAReactiveSpawner(); \
	friend REACTIVEFLOOR_API class UClass* Z_Construct_UClass_AReactiveSpawner(); \
public: \
	DECLARE_CLASS(AReactiveSpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ReactiveFloor"), NO_API) \
	DECLARE_SERIALIZER(AReactiveSpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAReactiveSpawner(); \
	friend REACTIVEFLOOR_API class UClass* Z_Construct_UClass_AReactiveSpawner(); \
public: \
	DECLARE_CLASS(AReactiveSpawner, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ReactiveFloor"), NO_API) \
	DECLARE_SERIALIZER(AReactiveSpawner) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AReactiveSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AReactiveSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReactiveSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReactiveSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReactiveSpawner(AReactiveSpawner&&); \
	NO_API AReactiveSpawner(const AReactiveSpawner&); \
public:


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReactiveSpawner(AReactiveSpawner&&); \
	NO_API AReactiveSpawner(const AReactiveSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReactiveSpawner); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReactiveSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AReactiveSpawner)


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_PRIVATE_PROPERTY_OFFSET
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_10_PROLOG
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_RPC_WRAPPERS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_INCLASS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_INCLASS_NO_PURE_DECLS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
