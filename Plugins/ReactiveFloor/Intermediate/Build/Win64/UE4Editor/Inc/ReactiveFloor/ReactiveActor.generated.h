// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REACTIVEFLOOR_ReactiveActor_generated_h
#error "ReactiveActor.generated.h already included, missing '#pragma once' in ReactiveActor.h"
#endif
#define REACTIVEFLOOR_ReactiveActor_generated_h

#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_RPC_WRAPPERS
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAReactiveActor(); \
	friend REACTIVEFLOOR_API class UClass* Z_Construct_UClass_AReactiveActor(); \
public: \
	DECLARE_CLASS(AReactiveActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ReactiveFloor"), NO_API) \
	DECLARE_SERIALIZER(AReactiveActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAReactiveActor(); \
	friend REACTIVEFLOOR_API class UClass* Z_Construct_UClass_AReactiveActor(); \
public: \
	DECLARE_CLASS(AReactiveActor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/ReactiveFloor"), NO_API) \
	DECLARE_SERIALIZER(AReactiveActor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AReactiveActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AReactiveActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReactiveActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReactiveActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReactiveActor(AReactiveActor&&); \
	NO_API AReactiveActor(const AReactiveActor&); \
public:


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AReactiveActor(AReactiveActor&&); \
	NO_API AReactiveActor(const AReactiveActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AReactiveActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AReactiveActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AReactiveActor)


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_PRIVATE_PROPERTY_OFFSET
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_10_PROLOG
#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_RPC_WRAPPERS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_INCLASS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_PRIVATE_PROPERTY_OFFSET \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_INCLASS_NO_PURE_DECLS \
	Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Final_Project_Test_Plugins_ReactiveFloor_Source_ReactiveFloor_Public_ReactiveActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
